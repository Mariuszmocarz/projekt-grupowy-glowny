using System;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    [Header("Ustawienia poruszania sie")]
    public float moveSpeed = 500f;
    public float moveMultiplier = 9f;
    public float sprintMultiplier = 12f;
    public float maxSpeed = 20f;
    public float sprintMaxSpeedModifier = 4f;
    public float drag = 230f;

    [Header("Ustawienia skoku")]
    public float jumpForce = 300f;
    public float jumpMultiplier = 4f;
    public float inAirMovementModifier = 0.8f;
    public float inAirDrag = 160f;

    [Header("Ustawienia sprawdzania podloza")]
    [SerializeField] private float groundCheckRadius = 0.4f;
    [SerializeField] private float groundCheckDistance = 1f;
    [SerializeField] private LayerMask groundLayer;

    [Header("Ustawienia wslizgu oraz kucania")]
    public float slideForce = 800f;
    public float slideDrag = 0.1f;
    public float slideCooldown = 1f;
    public float slideJumpMultiplier = 7f;
    public float crouchJumpMultiplier = 3f;
    public Vector3 crouchScale = new Vector3(1f, 0.5f, 1f);
    private float currentSlope = 0f;
    private float timeSinceLastSlide = Mathf.Infinity;
    private bool isSliding, isCrouching, isJumping, isSprinting, isWallRunning, isDoubleJumping;

    [Header("Ustawienia biegania po scianach")]
    [SerializeField] private LayerMask wallLayer;
    public float wallrunForce = 3000f;
    public float maxWallrunTime = 2f;
    public float maxWallSpeed = 30f;
    public float maxWallRunCameraTilt = 25f;
    public float wallRunCameraTilt = 0f;
    bool isWallRight, isWallLeft;

    [Header("Ustawienia uniku")]
    public float dodgeForce = 3000f;
    public float dodgeMultiplier = 10f;
    public float dodgeCooldown = 1f;
    private float timeSinceLastDodge = Mathf.Infinity;

    private InputMaster controls;

    private Rigidbody rb;
    private Camera cam;

    private Vector3 moveDirection;
    private Vector3 originalPlayerScale = Vector3.zero;
    private Vector2 moveInput = Vector2.zero;
    private Vector2 dodgeInput = Vector2.zero;

    private void Start()
    {
        controls = new InputMaster();
        controls.Enable();

        controls.Player.Movement.performed += ctx => moveInput = ctx.ReadValue<Vector2>();
        controls.Player.Jump.performed += ctx => {
            isJumping = true;
            Jump();
        };
        controls.Player.Jump.canceled += ctx => isJumping = false;
        controls.Player.Crouch.performed += ctx => ToggleCrouch(true);
        controls.Player.Crouch.canceled += ctx => ToggleCrouch(false);
        controls.Player.Sprint.performed += ctx => isSprinting = true;
        controls.Player.Sprint.canceled += ctx => isSprinting = false;
        controls.Player.DodgeLeft.performed += ctx => dodgeInput.x = 1;
        controls.Player.DodgeRight.performed += ctx => dodgeInput.y = 1;

        rb = GetComponent<Rigidbody>();
        cam = Camera.main;
        originalPlayerScale = transform.localScale;
    }

    private void Update()
    {
        CheckForWall();

        if (moveInput.x == 1 && isWallRight) { StartWallrun(); }
        if (moveInput.x == -1 && isWallLeft) { StartWallrun(); }

        if (timeSinceLastSlide < slideCooldown) { timeSinceLastSlide += Time.deltaTime; }
        if (timeSinceLastDodge < dodgeCooldown) { timeSinceLastDodge += Time.deltaTime; }
    }

    private void FixedUpdate()
    {
        Movement();
        ManageCamera();
    }

    private void Jump()
    {
        if (isWallRunning) {
            isJumping = false;

            if (isWallLeft && moveInput.x != 1 || isWallRight && moveInput.x != -1) {
                rb.AddForce(Vector2.up * jumpForce * (isCrouching && currentSlope < 15f ? crouchJumpMultiplier : isCrouching && currentSlope >= 15f ? slideJumpMultiplier : jumpMultiplier));
            }

            if (isWallRight || isWallLeft && moveInput.x == -1 || moveInput.x == 1) { rb.AddForce(-transform.up * jumpForce * 1f); }
            if (isWallRight && moveInput.x == -1) { rb.AddForce(-transform.right * jumpForce * 3.2f); }
            if (isWallLeft && moveInput.x == 1) { rb.AddForce(transform.right * jumpForce * 3.2f); }

            rb.AddForce(transform.forward * jumpForce * 1f);
        }

        if (GroundCheck() || !GroundCheck() && isDoubleJumping) {
            rb.AddForce(Vector2.up * jumpForce * (isCrouching && currentSlope < 15f ? crouchJumpMultiplier : isCrouching && currentSlope >= 15f ? slideJumpMultiplier : jumpMultiplier));
            isDoubleJumping = !isDoubleJumping;
        }
    }

    private void ToggleCrouch(bool enabled)
    {
        if (enabled) {
            isCrouching = true;
            transform.localScale = crouchScale;
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);

            if (rb.velocity.magnitude > 0.5f && GroundCheck() && !isSliding && timeSinceLastSlide >= slideCooldown) {
                rb.AddForce(transform.forward * slideForce);
                isSliding = true;
                timeSinceLastSlide = 0f;
            }
        }
        else
        {
            isCrouching = false;
            isSliding = false;
            transform.localScale = originalPlayerScale;
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
        }
    }

    private void Movement()
    {
        moveDirection = transform.right * moveInput.x + transform.forward * moveInput.y;

        ApplyDrag(-rb.velocity);

        if (isCrouching && GroundCheck())
        {
            rb.AddForce(Vector3.down * Time.fixedDeltaTime * 5000f);
            return;
        }

        if (dodgeInput.x == 1 && Mathf.Clamp01(timeSinceLastDodge) >= dodgeCooldown) {
            rb.AddForce(-transform.right * Time.fixedDeltaTime * dodgeForce * dodgeMultiplier);
            timeSinceLastDodge = 0f;
        }
        if (dodgeInput.y == 1 && Mathf.Clamp01(timeSinceLastDodge) >= dodgeCooldown) {
            rb.AddForce(transform.right * Time.fixedDeltaTime * dodgeForce * dodgeMultiplier);
            timeSinceLastDodge = 0f;
        }
        dodgeInput.x = 0;
        dodgeInput.y = 0;

        float multiplier = GroundCheck() ? 1f : inAirMovementModifier;

        if (rb.velocity.magnitude > (isSprinting ? maxSpeed + sprintMaxSpeedModifier : maxSpeed)) { moveDirection = Vector3.zero; }
        rb.AddForce(moveDirection * (moveSpeed * (isSprinting ? sprintMultiplier : moveMultiplier)) * Time.fixedDeltaTime * multiplier * ((GroundCheck() && isCrouching) ? 0.7f : multiplier));
    }

    private void ApplyDrag(Vector3 direction)
    {
        if (!GroundCheck() || isJumping)
        {
            rb.AddForce(new Vector3(direction.x, 0f, direction.z) * inAirDrag * Time.fixedDeltaTime);
            return;
        }

        if (isCrouching && currentSlope >= 15f || isSliding && timeSinceLastSlide < slideCooldown)
        {
            rb.AddForce(moveSpeed * Time.fixedDeltaTime * direction.normalized * slideDrag);
            return;
        }

        if (rb.velocity.magnitude != 0)
        {
            rb.AddForce(direction * drag * Time.fixedDeltaTime);
        }
    }

    private bool GroundCheck()
    {
        bool grounded = Physics.SphereCast(transform.position, groundCheckRadius, Vector3.down, out RaycastHit hit, groundCheckDistance, groundLayer);
        currentSlope = Vector3.Angle(Vector3.up, hit.normal);

        return grounded;
    }

    private void StartWallrun()
    {
        rb.useGravity = false;
        isWallRunning = true;

        if (rb.velocity.magnitude <= maxWallSpeed)
        {
            rb.AddForce(transform.forward * wallrunForce * Time.deltaTime);

            if (isWallRight) { rb.AddForce(transform.right * wallrunForce / 5 * Time.deltaTime); }
            else
            {
                rb.AddForce(-transform.right * wallrunForce / 5 * Time.deltaTime);
            }
        }
    }

    private void StopWallRun()
    {
        isWallRunning = false;
        rb.useGravity = true;
    }

    private void CheckForWall()
    {
        isWallRight = Physics.Raycast(transform.position, transform.right, 1f, wallLayer);
        isWallLeft = Physics.Raycast(transform.position, -transform.right, 1f, wallLayer);

        if (!isWallLeft && !isWallRight) { StopWallRun(); }
        if (isWallLeft || isWallRight) { isDoubleJumping = true; }
    }

    private void ManageCamera()
    {
        if (Math.Abs(wallRunCameraTilt) < maxWallRunCameraTilt && isWallRunning && isWallRight) { wallRunCameraTilt += Time.deltaTime * maxWallRunCameraTilt * 2; }
        if (Math.Abs(wallRunCameraTilt) < maxWallRunCameraTilt && isWallRunning && isWallLeft) { wallRunCameraTilt -= Time.deltaTime * maxWallRunCameraTilt * 2; }

        if (wallRunCameraTilt > 0 && !isWallRight && !isWallLeft) { wallRunCameraTilt -= Time.deltaTime * maxWallRunCameraTilt * 2; }
        if (wallRunCameraTilt < 0 && !isWallRight && !isWallLeft) { wallRunCameraTilt += Time.deltaTime * maxWallRunCameraTilt * 2; }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (GroundCheck() && isCrouching && rb.velocity.magnitude > 0.5f && currentSlope < 15f && !isSliding && timeSinceLastSlide >= slideCooldown)
        {
            rb.AddForce(transform.forward * slideForce);
        }
    }
}
